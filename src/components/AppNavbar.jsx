import React, { useState, useContext} from 'react';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import { Link, NavLink } from 'react-router-dom';
import UserContext from '../UserContext';
import './AppNavbar.css'





export default function AppNavbar() {
    const { user } = useContext(UserContext);

    return(
        <Navbar expand="lg" className='navBarBg'>
		  
		    <Navbar.Brand className="brandLogo" as={Link} to="/">DF Cosme</Navbar.Brand>
		    <Navbar.Toggle aria-controls="basic-navbar-nav" />
		    <Navbar.Collapse id="basic-navbar-nav" className='navBar' >
		      <Nav className="me-auto d-flex justify-content-end flex-grow-1 pe-5">
		        <Nav.Link as={Link} to="/" exact>Home</Nav.Link>
		        <Nav.Link as={Link} to="/products" exact>Products</Nav.Link>

				{(function(){
                        if(user.id !== null && user.isAdmin === false){
                            return (
                            <React.Fragment>
                                <Nav.Link as={Link} to="/logout" exact>Logout</Nav.Link>
                            </React.Fragment>
                            )
                        } else if(user.id !== null && user.isAdmin === true){
                            return (
                            <React.Fragment>
                                <Nav.Link as={Link} to="/adminView" exact>Admin</Nav.Link>
                                <Nav.Link as={Link} to="/logout" exact>Logout</Nav.Link>
                            </React.Fragment>
                            )
                        } else {
                            return (
                            <React.Fragment>
                                <Nav.Link as={Link} to="/login" exact>Login</Nav.Link>
                                <Nav.Link as={Link} to="/register" exact>Register</Nav.Link>
                            </React.Fragment>
                            )
                        }
                    })()}
				
		      </Nav>
		    </Navbar.Collapse>
		  
		</Navbar>
    )
}