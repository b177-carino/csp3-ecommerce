import { Col, Row, Container} from 'react-bootstrap';
import { Link } from 'react-router-dom';
import './Banner.css'

export default function Banner({data}){
    console.log(data);
    const {title, content, destination, label} = data; 

    return (
        <>
        <Row>
            <Col className="p-5">
                <h1 className='brandName'>{title}</h1> 
                <p>{content}</p>
                <Link to = {destination}>{label}</Link>
            </Col>
        </Row>
        </>
    )
}