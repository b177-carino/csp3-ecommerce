import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import { useParams, useNavigate, Link } from 'react-router-dom';
import UserContext from '../UserContext';

export default function ProductView() {

  const { user } = useContext(UserContext);

  const history = useNavigate;

  const { productId } = useParams();

  const [ name, setName ] = useState('');
  const [ description, setDescription] = useState('');
  const [ price, setPrice ] = useState(0)

      

      const purchase = (productId) => {
        fetch('https://tranquil-bayou-65537.herokuapp.com/users/checkout', {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
            Authorization : `Bearer ${localStorage.getItem('token')}`
          },
          body: JSON.stringify({
            productId: productId,
            userId: user.id,
          })
        })

        .then(data => {
          if(data){
            alert('You have successfully order')
          } else {
            alert("Something went wrong, please try again later")
          }
        })
      }

      useEffect(() => {
        console.log(productId);

        fetch(`https://tranquil-bayou-65537.herokuapp.com/products/${productId}`)
        .then(res => res.json())
        .then(data => {
          console.log(data);

          setName(data.name);
          setDescription(data.description);
          setPrice(data.price)
        })
      }, [productId]);
  

return (
    <Container fluid>
    <Row>
        <Card style={{ width: '18rem' }}>
          <Card.Body className="text-center">
            <Card.Title>{name}</Card.Title>
            <Card.Subtitle className="mb-2 text-muted">Description:</Card.Subtitle>
            <Card.Text>{description}</Card.Text>
            <Card.Subtitle className="mb-2 text-muted">Price:</Card.Subtitle>
            <Card.Text>Php {price}</Card.Text>
            { user.id !== null ?
                <Button variant="primary" onClick={() => purchase(productId)}>Purchase</Button>
                :
                <Link className="btn btn-danger btn-block" to="/login">Log in to Purchase</Link>
            }
            
          </Card.Body>
        </Card>
    </Row>
</Container>
    )
}