import { Fragment, useEffect, useState, useContext } from 'react';
import ProductCard from '../components/ProductCard';
import UserContext from '../UserContext';

export default function Products() {

    
    const [products, setProducts] = useState([]);


    const fetchData = () => {
        fetch('https://tranquil-bayou-65537.herokuapp.com/products')
        .then(res => res.json())
        .then(data => {
            console.log(data);


            setProducts(data.map(product =>{

                return (
                    <ProductCard key={product._id} productProp={product} />
                );
            }))
        })
    }

    useEffect(() => {
        fetchData()
    }, []);

    return(
        <Fragment>
            {products}
        </Fragment>
    )
}